# Overall Goal

This library contains all the enums, structs, and message types in order to let the remote communicate with the trailer reliably.

## Plan

This library should allow for both sending and recieving serialization. Each component should be able to request a channel and then drop a Message type in to get that sent to either the remote or the trailer. The message type can then be interpretted to get the appropriate struct out.
