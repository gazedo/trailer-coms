#![no_std]
// We always pull in `std` during tests, because it's just easier
// to write tests when you can assume you're on a capable platform
#[cfg(any(feature = "std", test))]
#[macro_use]
extern crate std;

// When we're building for a no-std target, we pull in `core`, but alias
// it as `std` so the `use` statements are the same between `std` and `core`.
#[cfg(all(not(feature = "std"), not(test)))]
#[macro_use]
extern crate core as std;

extern crate alloc;
use alloc::vec::Vec;

use num_enum::{IntoPrimitive, TryFromPrimitive, TryFromPrimitiveError};

//Statically define the possible destinations
pub static BROADCAST: [u8; 6] = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];
#[allow(unused)]
pub static TRAILER: [u8; 6] = [0x60, 0x55, 0xF9, 0x57, 0xAA, 0x88];
pub static REMOTES: [[u8; 6]; 1] = [[0x30, 0xC6, 0xf7, 0x25, 0xed, 0x8d]];

#[derive(Debug, PartialEq)]
pub enum ComsErrors {
    ParseError,
    NotImplemented,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Battery {
    pub id: Id,
    pub soc: f32,
    pub voltage: f32,
    pub current: f32,
    pub temperature: f32,
}

#[derive(Debug, PartialEq, Clone)]
pub struct TankLevel {
    pub id: Id,
    pub amount: f32,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct RelayCMD {
    pub id: Id,
    pub state: bool,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Imu {
    pub id: Id,
    pub roll: f32,
    pub pitch: f32,
}

// #[derive(Debug, PartialEq)]
// pub struct Zero {
//     pub id: Id,
//     pub cmd: bool,
// }

#[derive(Debug, PartialEq, Clone)]
pub struct UpdateRequest {
    pub id: Id,
    pub ids: Vec<Id>,
}

// pub enum
pub trait Serialize {
    // Struct -> MessagePack
    fn pack(&self, mac: &[u8; 6]) -> Message;
    // Go from serialized data to struct
    fn unpack(msg: Message) -> Result<Self, ComsErrors>
    where
        Self: Sized;
}
pub trait GetState {
    // pub trait GetState {
    type Output;
    fn get_id(&self) -> Result<Id, ComsErrors>;
    fn get_state(&self) -> Result<Self::Output, ComsErrors>;
}
#[derive(Debug, Eq, PartialEq, TryFromPrimitive, IntoPrimitive, Clone, Copy, Hash)]
#[repr(u8)]
pub enum Id {
    Update,
    StoveLight,
    PrepLight,
    Fridge,
    WaterPump,
    Battery,
    Imu,
    WaterLevel,
    PropaneLevel,
    Zero,
    Error,
}
#[derive(Debug, PartialEq)]
pub struct Message {
    mac: [u8; 6],
    data: Vec<u8>,
}

impl Message {
    pub fn new(mac_address: &[u8], data: &[u8]) -> Self {
        let mac = mac_address.try_into().expect("Mac address wrong length");
        Message {
            mac,
            data: data.to_vec(),
        }
    }
    pub fn get_mac(&self) -> [u8; 6] {
        return self.mac.clone();
    }
    pub fn get_message(&self) -> Vec<u8> {
        return self.data.clone();
    }
    pub fn get_id(&self) -> Result<Id, TryFromPrimitiveError<Id>> {
        // pub fn get_id(&self) -> Id {
        let id = self.data[0].try_into();
        return id;
    }
}

impl Serialize for RelayCMD {
    fn pack(&self, mac: &[u8; 6]) -> Message {
        let mut msg = Message {
            mac: mac.clone(),
            data: Vec::new(),
        };
        msg.data.push(self.id.into());
        msg.data.push(self.state.into());
        msg
    }
    fn unpack(msg: Message) -> Result<Self, ComsErrors> {
        if msg.data.len() != 2 {
            return Err(ComsErrors::ParseError);
        }
        let id = msg.data[0].try_into().unwrap();
        let state = if msg.data[1] == 1 { true } else { false };
        Ok(RelayCMD { id, state })
    }
}

impl GetState for RelayCMD {
    type Output = bool;
    fn get_id(&self) -> Result<Id, ComsErrors> {
        Ok(self.id)
    }
    fn get_state(&self) -> Result<Self::Output, ComsErrors> {
        Ok(self.state)
    }
}

// impl Serialize for Zero {
//     fn pack(&self, mac: &[u8; 6]) -> Message {
//         let mut msg = Message {
//             mac: mac.clone(),
//             data: Vec::new(),
//         };
//         msg.data.push(self.id.into());
//         msg.data.extend(self.cmd.to_be_bytes());
//         msg
//     }
//     fn unpack(msg: Message) -> Result<Self, ComsErrors> {
//         if msg.data.len() != 5 {
//             return Err(ComsErrors::ParseError);
//         }
//         let id = msg.data[0].try_into().unwrap();
//         let mut buf = [0; 4];
//         for i in 0..4 {
//             buf[i] = msg.data[i + 1];
//         }

//         let cmd = f32::from_be_bytes(buf);
//         Ok(Zero { id, amount })
//     }
// }
impl Serialize for TankLevel {
    fn pack(&self, mac: &[u8; 6]) -> Message {
        let mut msg = Message {
            mac: mac.clone(),
            data: Vec::new(),
        };
        msg.data.push(self.id.into());
        msg.data.extend(self.amount.to_be_bytes());
        msg
    }
    fn unpack(msg: Message) -> Result<Self, ComsErrors> {
        if msg.data.len() != 5 {
            return Err(ComsErrors::ParseError);
        }
        let id = msg.data[0].try_into().unwrap();
        let mut buf = [0; 4];
        for i in 0..4 {
            buf[i] = msg.data[i + 1];
        }

        let amount = f32::from_be_bytes(buf);
        Ok(TankLevel { id, amount })
    }
}
impl GetState for TankLevel {
    type Output = f32;
    fn get_state(&self) -> Result<Self::Output, ComsErrors> {
        Ok(self.amount)
    }
    fn get_id(&self) -> Result<Id, ComsErrors> {
        Ok(self.id)
    }
}
impl Serialize for Imu {
    fn pack(&self, mac: &[u8; 6]) -> Message {
        let mut msg = Message {
            mac: mac.clone(),
            data: Vec::new(),
        };
        msg.data.push(Id::Imu.into());
        msg.data.extend(self.roll.to_be_bytes());
        msg.data.extend(self.pitch.to_be_bytes());
        msg
    }
    fn unpack(msg: Message) -> Result<Self, ComsErrors> {
        if msg.data.len() != 9 {
            return Err(ComsErrors::ParseError);
        }
        let mut buf = [0; 4];
        for i in 0..4 {
            buf[i] = msg.data[i + 1];
        }
        let roll = f32::from_be_bytes(buf);
        for i in 0..4 {
            buf[i] = msg.data[i + 5];
        }
        let pitch = f32::from_be_bytes(buf);
        Ok(Imu {
            id: Id::Imu,
            roll,
            pitch,
        })
    }
}
impl GetState for Imu {
    type Output = (f32, f32);
    fn get_state(&self) -> Result<Self::Output, ComsErrors> {
        Ok((self.roll, self.pitch))
    }
    fn get_id(&self) -> Result<Id, ComsErrors> {
        Ok(self.id)
    }
}

impl Serialize for Battery {
    fn pack(&self, mac: &[u8; 6]) -> Message {
        let mut msg = Message {
            mac: mac.clone(),
            data: Vec::new(),
        };
        msg.data.push(Id::Battery.into());
        msg.data.extend(self.soc.to_be_bytes());
        msg.data.extend(self.voltage.to_be_bytes());
        msg.data.extend(self.current.to_be_bytes());
        msg.data.extend(self.temperature.to_be_bytes());
        msg
    }
    fn unpack(msg: Message) -> Result<Self, ComsErrors> {
        if msg.data.len() != 17 {
            return Err(ComsErrors::ParseError);
        }
        let mut buf = [0; 4];
        for i in 0..4 {
            buf[i] = msg.data[i + 1];
        }
        let soc = f32::from_be_bytes(buf);
        for i in 0..4 {
            buf[i] = msg.data[i + 5];
        }
        let voltage = f32::from_be_bytes(buf);
        for i in 0..4 {
            buf[i] = msg.data[i + 9];
        }
        let current = f32::from_be_bytes(buf);
        for i in 0..4 {
            buf[i] = msg.data[i + 13];
        }
        let temperature = f32::from_be_bytes(buf);
        Ok(Battery {
            id: Id::Battery,
            soc,
            voltage,
            current,
            temperature,
        })
    }
}

impl GetState for Battery {
    type Output = (f32, f32, f32);
    fn get_state(&self) -> Result<Self::Output, ComsErrors> {
        Ok((self.soc, self.voltage, self.current))
    }
    fn get_id(&self) -> Result<Id, ComsErrors> {
        Ok(self.id)
    }
}

impl Serialize for UpdateRequest {
    fn pack(&self, mac: &[u8; 6]) -> Message {
        let mut msg = Message {
            mac: mac.clone(),
            data: Vec::new(),
        };
        msg.data.push(Id::Update.into());
        for id in self.ids.iter() {
            msg.data.push(id.clone().into());
        }
        msg
    }
    fn unpack(msg: Message) -> Result<Self, ComsErrors>
    where
        Self: Sized,
    {
        if msg.data[0] != Id::Update.into() {
            return Err(ComsErrors::ParseError);
        }

        let mut buf: Vec<Id> = Vec::new();
        for i in msg.data.into_iter() {
            buf.push(i.try_into().unwrap());
        }
        Ok(UpdateRequest {
            id: Id::Update,
            ids: buf[1..].to_vec(),
        })
    }
}
// impl GetState<Vec<Id>> for UpdateRequest {
impl GetState for UpdateRequest {
    type Output = Vec<Id>;
    fn get_state(&self) -> Result<Vec<Id>, ComsErrors> {
        Ok(self.ids.clone())
    }
    fn get_id(&self) -> Result<Id, ComsErrors> {
        Ok(self.id)
    }
}

#[cfg(test)]
mod tests {
    use alloc::vec::Vec;

    use crate::{
        Battery, ComsErrors, Id, Imu, Message, RelayCMD, Serialize, TankLevel, UpdateRequest,
        BROADCAST, TRAILER,
    };

    #[test]
    fn parse_correct_water() {
        let amount = 30.0_f32;
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff: Vec<u8> = vec![Id::WaterLevel.into()];
        data_buff.extend(amount_array.iter());

        let msg = Message {
            mac: BROADCAST,
            data: data_buff,
        };

        let decode = TankLevel::unpack(msg);
        match decode {
            Ok(obj) => {
                assert!(obj.id == Id::WaterLevel);
                assert!(obj.amount == amount);
            }
            Err(_) => assert!(false),
        }
    }
    #[test]
    fn parse_correct_propane() {
        let amount = 30.0_f32;
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff: Vec<u8> = vec![Id::PropaneLevel.into()];
        data_buff.extend(amount_array.iter());

        let msg = Message {
            mac: BROADCAST,
            data: data_buff,
        };

        let decode = TankLevel::unpack(msg);
        match decode {
            Ok(obj) => {
                assert!(obj.id == Id::PropaneLevel);
                assert!(obj.amount == amount);
            }
            Err(_) => assert!(false),
        }
    }
    #[test]
    #[should_panic]
    fn parse_wrong() {
        let amount = 30.0_f32;
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff: Vec<u8> = vec![(Id::PropaneLevel).into()];
        data_buff.extend(amount_array.iter());

        let mut msg = Message {
            mac: BROADCAST,
            data: data_buff,
        };

        msg.data[0] = 20u8;

        let decode = TankLevel::unpack(msg);
        match decode {
            Ok(_) => {
                panic!("Got a valid object back when should have failed")
            }
            Err(e) => assert_eq!(e, ComsErrors::ParseError),
        }
    }

    #[test]
    fn check_members() {
        let amount = 30.0_f32;
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff_id: Vec<u8> = vec![Id::Battery.into()];
        data_buff_id.extend(amount_array.iter());
        let data_clone = data_buff_id.clone();
        let msg_wrong = Message {
            mac: TRAILER,
            data: data_buff_id,
        };
        assert_eq!(msg_wrong.get_mac(), TRAILER);
        assert_eq!(msg_wrong.get_message(), data_clone);
        assert_eq!(msg_wrong.get_id().unwrap(), Id::Battery);
    }
    #[test]
    #[should_panic]
    fn check_members_fail() {
        let amount = 30.0_f32;
        let wrong_id = 50u8;
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff_id: Vec<u8> = vec![Id::Battery.into()];
        let data_close = data_buff_id.clone();
        data_buff_id.extend(amount_array.iter());
        data_buff_id[0] = wrong_id;
        let msg_wrong = Message {
            mac: BROADCAST,
            data: data_buff_id,
        };
        assert_eq!(msg_wrong.get_mac(), TRAILER);
        assert_eq!(msg_wrong.get_message(), data_close);
        let _id = msg_wrong.get_id();
    }
    #[test]
    fn encode_test() {
        let amount = 30_f32;
        let tl = TankLevel {
            id: Id::PropaneLevel,
            amount,
        };
        let msg = tl.pack(&BROADCAST);
        let amount_array = amount.to_be_bytes().to_vec();
        let mut data_buff: Vec<u8> = vec![Id::PropaneLevel.into()];
        data_buff.extend(amount_array.iter());

        let msg_manual = Message {
            mac: BROADCAST,
            data: data_buff,
        };
        assert_eq!(msg, msg_manual);
    }
    #[test]
    fn encode_decode_imu() {
        let imu = Imu {
            id: Id::Imu,
            roll: 3.0,
            pitch: -5.2,
        };
        let msg = imu.pack(&BROADCAST);

        let unpack_imu = Imu::unpack(msg).unwrap();

        assert_eq!(imu, unpack_imu);
    }
    #[test]
    fn encode_decode_battery() {
        let batt = Battery {
            id: Id::Battery,
            soc: 94.58,
            voltage: 12.3,
            current: -1.3,
            temperature: 76.4,
        };
        let msg = batt.pack(&BROADCAST);

        let unpack_batt = Battery::unpack(msg).unwrap();

        assert_eq!(batt, unpack_batt);
    }
    #[test]
    fn encode_decode_relay() {
        let r = RelayCMD {
            id: Id::Fridge,
            state: true,
        };
        let msg = r.pack(&BROADCAST);

        let unpack_r = RelayCMD::unpack(msg).unwrap();

        assert_eq!(r, unpack_r);
    }
    #[test]
    fn encode_decode_updaterequest() {
        let r = UpdateRequest {
            id: Id::Update,
            ids: vec![Id::PrepLight, Id::StoveLight, Id::Fridge],
        };
        let msg = r.pack(&BROADCAST);

        let unpack_r = UpdateRequest::unpack(msg).unwrap();

        assert_eq!(r, unpack_r);
    }
}
